package br.com.upis.serialization;

import br.com.upis.model.Horario;

import java.io.*;

public class Serialization {

    public static void main(String[] args) throws IOException {
        Horario horario = new Horario(12,0, 0);


        try {
            serialize(horario, "horario-obj");
            Horario horario1 = deserialize("horario-obj");

            System.out.println(horario1.toString());
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }

    }


    public static void serialize(Horario horario, String fileName) throws IOException {

        FileOutputStream fileOutputStream = new FileOutputStream("src/main/resources/" + fileName);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

        objectOutputStream.writeObject(horario);

        fileOutputStream.close();
        objectOutputStream.close();
    }

    public static Horario deserialize(String fileName) throws IOException, ClassNotFoundException {

        FileInputStream fileInputStream = new FileInputStream("src/main/resources/" + fileName);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);


        return (Horario) objectInputStream.readObject();
    }
}
